<?php

namespace Chattle\Console;

use Illuminate\Console\Command;

class PublishCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chattle:publish {--force : Overwrite any existing files}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish all of the chattle assets';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if($this->option('force')){
            $this->call('vendor:publish', [
                '--tag' => 'chattle-config',
                '--force' => true,
            ]);

            $this->call('vendor:publish', [
                '--tag' => 'chattle-migrations',
                '--force' => true,
            ]);

            $this->call('vendor:publish', [
                '--tag' => 'chattle-models',
                '--force' => true,
            ]);
        }

        $this->call('vendor:publish', [
            '--tag' => 'chattle-views',
            '--force' => true,
        ]);

        $this->call('vendor:publish', [
            '--tag' => 'chattle-assets',
            '--force' => true,
        ]);
    }
}
