<?php

namespace Chattle\Facades;

use Illuminate\Support\Facades\Facade;

class ChattleMessenger extends Facade
{

    protected static function getFacadeAccessor()
    {
       return 'ChattleMessenger';
    }
}
