<title>{{ config('chattle.name') }}</title>

{{-- Meta tags --}}
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="channel_id" content="{{ $channel_id }}">
<meta name="messenger-color" content="#4CAF50">
<meta name="messenger-theme" content="{{ auth()->user()->dark_mode }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta
    name="url" content="{{ url('').'/'.config('chattle.routes.prefix') }}"
    data-auth-user="{{ Auth::user()->id }}"
    data-auth-channel="{{ Auth::user()->channel_id }}"
>

{{-- scripts --}}
<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="{{ asset('js/chattle/font.awesome.min.js') }}"></script>
<script src="{{ asset('js/chattle/autosize.js') }}"></script>
{{--<script src="{{ asset('js/app.js') }}"></script>--}}
<script src='https://unpkg.com/nprogress@0.2.0/nprogress.js'></script>

{{-- styles --}}
<link rel='stylesheet' href='https://unpkg.com/nprogress@0.2.0/nprogress.css'/>
<link href="{{ asset('css/chattle/style.css') }}" rel="stylesheet"/>
<link href="{{ asset('css/chattle/'.$dark_mode.'.mode.css') }}" rel="stylesheet"/>
{{--<link href="{{ asset('css/app.css') }}" rel="stylesheet" />--}}

{{-- Setting messenger primary color to css --}}
<style>
    :root {
        --primary-color: #4CAF50;
    }
</style>
