<script src="https://js.pusher.com/7.2.0/pusher.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@joeattardi/emoji-button@3.0.3/dist/index.min.js"></script>
<script >
    // Gloabl Chattle variables from PHP to JS
    window.chattle = {
        name: "{{ config('chattle.name') }}",
        sounds: {!! json_encode(config('chattle.sounds')) !!},
        allowedImages: {!! json_encode(config('chattle.attachments.allowed_images')) !!},
        allowedFiles: {!! json_encode(config('chattle.attachments.allowed_files')) !!},
        maxUploadSize: {{ Chattle::getMaxUploadSize() }},
        pusher: {!! json_encode(config('chattle.pusher')) !!},
        pusherAuthEndpoint: '{{route("pusher.auth")}}'
    };
    window.chattle.allAllowedExtensions = chattle.allowedImages.concat(chattle.allowedFiles);
</script>
<script src="{{ asset('js/chattle/utils.js') }}"></script>
<script src="{{ asset('js/chattle/code.js') }}"></script>
