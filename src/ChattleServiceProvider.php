<?php

namespace Chattle;

use Chattle\Console\InstallCommand;
use Chattle\Console\PublishCommand;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ChattleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind('ChattleMessenger', function () {
            return new ChattleMessenger;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Load Views and Routes
        $this->loadViewsFrom(__DIR__ . '/views', 'Chattle');
        $this->loadRoutes();

        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class,
                PublishCommand::class,
            ]);
            $this->setPublishes();
        }
    }

    /**
     * Publishing the files that the user may override.
     *
     * @return void
     */
    protected function setPublishes()
    {
        // Load user's avatar folder from package's config
        $userAvatarFolder = json_decode(json_encode(include(__DIR__ . '/config/chattle.php')))->user_avatar->folder;

        // Config
        $this->publishes([
            __DIR__ . '/config/chattle.php' => config_path('chattle.php')
        ], 'chattle-config');

        // Migrations
        $this->publishes([
            __DIR__ . '/database/migrations/2022_01_10_99999_add_active_status_to_users.php' => database_path('migrations/' . date('Y_m_d') . '_000000_add_active_status_to_users.php'),
            __DIR__ . '/database/migrations/2022_01_10_99999_add_avatar_to_users.php' => database_path('migrations/' . date('Y_m_d') . '_000000_add_avatar_to_users.php'),
            __DIR__ . '/database/migrations/2022_01_10_99999_add_channel_id_to_users.php' => database_path('migrations/' . date('Y_m_d') . '_000000_add_channel_id_to_users.php'),
            __DIR__ . '/database/migrations/2022_01_10_99999_add_dark_mode_to_users.php' => database_path('migrations/' . date('Y_m_d') . '_000000_add_dark_mode_to_users.php'),
            __DIR__ . '/database/migrations/2022_01_10_99999_create_chattle_channels_table.php' => database_path('migrations/' . date('Y_m_d') . '_000000_create_chattle_channels_table.php'),
            __DIR__ . '/database/migrations/2022_01_10_99999_create_chattle_favorites_table.php' => database_path('migrations/' . date('Y_m_d') . '_000000_create_chattle_favorites_table.php'),
            __DIR__ . '/database/migrations/2022_01_10_99999_create_chattle_messages_table.php' => database_path('migrations/' . date('Y_m_d') . '_000000_create_chattle_messages_table.php'),
            __DIR__ . '/database/migrations/2022_01_10_99999_create_chattle_channel_user_table.php' => database_path('migrations/' . date('Y_m_d') . '_000001_create_chattle_channel_user_table.php'),
        ], 'chattle-migrations');

        // Models
        $isV8 = explode('.', app()->version())[0] >= 8;
        $this->publishes([
            __DIR__ . '/Models' => app_path($isV8 ? 'Models' : '')
        ], 'chattle-models');

        // Controllers
        $this->publishes([
            __DIR__ . '/Http/Controllers' => app_path('Http/Controllers/vendor/Chattle')
        ], 'chattle-controllers');

        // Views
        $this->publishes([
            __DIR__ . '/views' => resource_path('views/vendor/Chattle')
        ], 'chattle-views');

        // Assets
        $this->publishes([
            // CSS
            __DIR__ . '/assets/css' => public_path('css/chattle'),
            // JavaScript
            __DIR__ . '/assets/js' => public_path('js/chattle'),
            // Images
            __DIR__ . '/assets/imgs' => storage_path('app/public/' . $userAvatarFolder),
             // CSS
             __DIR__ . '/assets/sounds' => public_path('sounds/chattle'),
        ], 'chattle-assets');

        // Routes (API and Web)
        $this->publishes([
            __DIR__ . '/routes' => base_path('routes/chattle')
        ], 'chattle-routes');
    }

    /**
     * Group the routes and set up configurations to load them.
     *
     * @return void
     */
    protected function loadRoutes()
    {
        Route::group($this->routesConfigurations(), function () {
            $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        });
        Route::group($this->apiRoutesConfigurations(), function () {
            $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
        });
    }

    /**
     * Routes configurations.
     *
     * @return array
     */
    private function routesConfigurations()
    {
        return [
            'prefix' => config('chattle.routes.prefix'),
            'namespace' =>  config('chattle.routes.namespace'),
            'middleware' => config('chattle.routes.middleware'),
        ];
    }
    /**
     * API routes configurations.
     *
     * @return array
     */
    private function apiRoutesConfigurations()
    {
        return [
            'prefix' => config('chattle.api_routes.prefix'),
            'namespace' =>  config('chattle.api_routes.namespace'),
            'middleware' => config('chattle.api_routes.middleware'),
        ];
    }
}
