<?php

return [
    'favorites' => 'Favorites',
    'search' => 'Search',
    'all_messages' => 'All messages',
    'connected' => 'Connected',
    'connecting' => 'Connecting',
    'no_internet_access' => 'No internet access',
    'select_chat' => 'Please Select a chat',
    'saved' => 'Saved Messages',
    'attachment' => 'Attachment',
    'group_leave' => 'Are you sure you want to leave this group?',
    'cancel' => 'Cancel',
    'leave' => 'Leave',
    'delete' => 'Are you sure you want to delete this?',
    'group_users' => 'Users in this group',
    'group_delete' => 'Are you sure you want to delete this group?',
    'delete_conversation' => 'Are you sure you want to delete this conversation?',
    'photos' => 'Shared Photos'
];
