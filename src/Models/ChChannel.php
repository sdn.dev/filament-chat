<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChChannel extends Model
{
    protected $fillable = [
        'avatar'
    ];

	public function users(){
		return $this->belongsToMany(User::class, 'ch_channel_user', 'channel_id', 'user_id');
	}
}
